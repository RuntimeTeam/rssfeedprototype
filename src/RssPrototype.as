package
{
	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.system.Capabilities;
	
	import feathers.themes.AeonDesktopTheme;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.textures.RenderTexture;
	import starling.utils.AssetManager;
	import starling.utils.RectangleUtil;
	import starling.utils.ScaleMode;
	import starling.utils.SystemUtil;
	
	[SWF(width="800", height="600", frameRate="60", backgroundColor="#000000")]
	public class RssPrototype extends Sprite
	{
		private var _mStarling:Starling;
		private var _theme:AeonDesktopTheme;
		public function RssPrototype()
		{
			if(this.stage)
			{
				init();
			} else
			{
				this.addEventListener(flash.events.Event.ADDED_TO_STAGE, init);
			}
		}
		
		// **************************************
		//	PUBLIC METHODS
		// **************************************
		
		
		// **************************************
		//	PRIVATE METHODS
		// **************************************
		
		private function init(e:flash.events.Event = null):void
		{
			this.removeEventListener(flash.events.Event.ADDED_TO_STAGE, init);
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			configInit();
			starlingInit();
		}
		
		private function starlingInit():void
		{
			var stageSize:Rectangle  = new Rectangle(0, 0, AppConfig.StageWidth, AppConfig.StageHeight);
			var screenSize:Rectangle = new Rectangle(0, 0, stage.stageWidth, stage.stageHeight);
			var viewPort:Rectangle = RectangleUtil.fit(stageSize, screenSize, ScaleMode.NO_BORDER);
			//			var scaleFactor:int = viewPort.width <= 700 ? 1 : 2;
			var scaleFactor:int = 2;
			//			var scaleFactor:int = 4;
			
			// Starling Setup
			Starling.multitouchEnabled = true; // useful on mobile devices
			Starling.handleLostContext = true; // recommended everywhere when using AssetManager
			RenderTexture.optimizePersistentBuffers = true; // safe on iOS, dangerous on Android
			
			_mStarling = new Starling(AppRoot, stage, viewPort, null, "auto", "auto");
			_mStarling.stage.stageWidth    = AppConfig.StageWidth;  // <- same size on all devices!
			_mStarling.stage.stageHeight   = AppConfig.StageHeight; // <- same size on all devices!
			_mStarling.enableErrorChecking = Capabilities.isDebugger;
			_mStarling.addEventListener(starling.events.Event.CONTEXT3D_CREATE, function():void
			{
				//				trace("Contect Created");
			});
			
			_mStarling.addEventListener(starling.events.Event.ROOT_CREATED, function():void
			{
				loadAssets(uiInit);
			});
			
			// Strart Starling
			_mStarling.start();
			
			// When the game becomes inactive, we pause Starling; otherwise, the enter frame event
			// would report a very long 'passedTime' when the app is reactivated.
			if (!SystemUtil.isDesktop)
			{
				NativeApplication.nativeApplication.addEventListener(
					flash.events.Event.ACTIVATE, function (e:*):void { _mStarling.start(); });
				NativeApplication.nativeApplication.addEventListener(
					flash.events.Event.DEACTIVATE, function (e:*):void { _mStarling.stop(true); });
			}
		}
		
		private function configInit():void
		{
			
		}
		
		private function uiInit(assets:AssetManager):void
		{
			_theme = new AeonDesktopTheme();
			
			var appRoot:AppRoot = _mStarling.root as AppRoot;
			appRoot.start(assets);
		}
		
		private function loadAssets(onComplete:Function):void
		{
			// Our assets are loaded and managed by the 'AssetManager'. To use that class,
			// we first have to enqueue pointers to all assets we want it to load.
			
			var assets:AssetManager = new AssetManager();
			
			assets.verbose = Capabilities.isDebugger;
			assets.enqueue("textures");
			
			assets.loadQueue(function(ratio:Number):void
			{
				if (ratio == 1) onComplete(assets);
			});
		}
	}
}