package com.proto.rss.model
{
	public class FeedItemVO extends AbstractJsonVO
	{
		public var description:String;
		public var link:String;
		public var title:String;
		
		public function FeedItemVO()
		{
			super();
		}
	}
}