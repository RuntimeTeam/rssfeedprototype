package com.proto.rss.model
{
	public class FeedVO extends AbstractJsonVO
	{
		public var name:String;
		public var feedUrl:String;
		
		public function FeedVO()
		{
			super();
		}
	}
}