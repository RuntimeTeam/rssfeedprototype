package com.proto.rss.model
{
	import flash.utils.describeType;
	import flash.utils.getQualifiedClassName;
	
	public class AbstractJsonVO extends Object
	{
		public function AbstractJsonVO()
		{
		}
		
		public function parse(o:Object):*
		{
			var typeDescription:XML = describeType(this);
			for each (var variable:XML in typeDescription.variable)
			{
				var propertyName:String = variable.@name.toString();
				var access:String = variable.@access.toString(); 
				var nonSerializedMetaData:XMLList = variable.metadata.(@name == "NonSerialized");
				if (access != "writeonly" && nonSerializedMetaData.length() == 0)
				{
					if(o[propertyName] != null)
					{
						this[propertyName] = o[propertyName];
					}
				}
			}
			return this;
		}
		
		public function toObject():Object
		{
			var obj:Object = {};
			var typeDescription:XML = describeType(this);
			for each (var variable:XML in typeDescription.variable)
			{
				var propertyName:String = variable.@name.toString();
				var access:String = variable.@access.toString(); 
				var nonSerializedMetaData:XMLList = variable.metadata.(@name == "NonSerialized");
				if (access != "writeonly" && nonSerializedMetaData.length() == 0)
				{
//					if(o[propertyName] != null)
//					{
						obj[propertyName] = this[propertyName];
//					}
				}
			}
			return obj;
		}
	}
}
