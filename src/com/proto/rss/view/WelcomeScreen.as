package com.proto.rss.view
{
	import com.proto.rss.model.FeedVO;
	
	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.controls.ScrollContainer;
	import feathers.controls.TextInput;
	
	import starling.events.Event;

	public class WelcomeScreen extends AbstractScreen
	{
		private var _container:ScrollContainer;
		private var _inputNewTitle:TextInput;
		private var _btnAdd:Button;
		private var _inputNewFeedUrl:TextInput;
		public function WelcomeScreen()
		{
			super();
			
			var header:Label = new Label();
			header.wordWrap = true;
			header.nameList.add(Label.ALTERNATE_STYLE_NAME_HEADING);
			header.text = "BCC News RSS Feed Reader prototype";
			header.width = 300;
			header.validate();
			header.x = (AppConfig.StageWidth >> 1) - (header.width >> 1);
			this.addChild(header);
			
			this.addChild(_container = new HScrollContainer());
			_container.touchable = true;
			_container.y = header.height + 10;;
			_container.width = 340;
			_container.height = 300;
			_container.x = (AppConfig.StageWidth >> 1) - (_container.width >> 1);
			var feeds:Array = AppConfig.FEED_LIST;
			
			for (var i:int = 0; i < feeds.length; i++) 
			{
				addFeedButton(feeds[i]);
			}
			
			// Input
			var lblAddNewFeed:Label = new Label();
			lblAddNewFeed.wordWrap = true;
			lblAddNewFeed.text = "Add your own feed here.";
			lblAddNewFeed.validate();
			lblAddNewFeed.y = _container.y + _container.height + 5;
			lblAddNewFeed.x = (AppConfig.StageWidth >> 1) - (lblAddNewFeed.width >> 1);
			this.addChild(lblAddNewFeed);
			
			this.addChild(_inputNewTitle = new TextInput());
			_inputNewTitle.width = 200;
			_inputNewTitle.height = 20;
			_inputNewTitle.prompt = "Feed Title";
			_inputNewTitle.y = lblAddNewFeed.y + lblAddNewFeed.height + 5;
			_inputNewTitle.x = (AppConfig.StageWidth >> 1) - (_inputNewTitle.width >> 1);
			
			this.addChild(_inputNewFeedUrl = new TextInput());
			_inputNewFeedUrl.width = 200;
			_inputNewFeedUrl.height = 20;
			_inputNewFeedUrl.prompt = "Feed URL";
			_inputNewFeedUrl.y = _inputNewTitle.y + _inputNewTitle.height + 5;
			_inputNewFeedUrl.x = (AppConfig.StageWidth >> 1) - (_inputNewFeedUrl.width >> 1);
			
			this.addChild(_btnAdd = new Button());
			_btnAdd.label = "Add this feed";
			_btnAdd.width = 150;
			_btnAdd.height = 30;
			_btnAdd.y = _inputNewFeedUrl.y + _inputNewFeedUrl.height + 5;
			_btnAdd.x = (AppConfig.StageWidth >> 1) - (_btnAdd.width >> 1);
			_btnAdd.addEventListener(Event.TRIGGERED, onTriggerAddFeed);
			
			this.addEventListener(Event.TRIGGERED, onTrigger);
		}
		
		private function addFeedButton(feed:Object):void
		{
			// TODO Auto Generated method stub
			var btn:Button;
			_container.addChild(btn = new RssFeedButton(new FeedVO().parse(feed)));
			btn.addEventListener(Event.TRIGGERED, onTrigger);
			btn.width = 150;
			btn.height = 50;
		}		
		
		// **************************************
		//	PUBLIC METHODS
		// **************************************
		
		override public function init(params:Object):void
		{
			
		}
			
		override public function start():void
		{
			
		}
		
		// **************************************
		//	PRIVATE METHODS
		// **************************************
		
		private function onTrigger(e:Event):void
		{
			var btn:RssFeedButton = e.target as RssFeedButton;
			if(btn)
			{
				AppRoot.instance.setState(AppRoot.STATE_FEEDS, btn.feed);
			}
		}
		
		private function onTriggerAddFeed(e:Event):void
		{
			if(_inputNewTitle.text.length > 0 && validateUrl(_inputNewFeedUrl.text))
			{
				var obj:Object = {name:_inputNewTitle.text, feedUrl:_inputNewFeedUrl.text};
				AppConfig.FEED_LIST.push(obj);
				addFeedButton(obj);
			}
		}
		
		private function validateUrl(url:String):Boolean
		{
			var pattern:RegExp = /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&:\/~\+#]*[\w\-\@?^=%&\/~\+#])?/;
			return pattern.test(url);
		}
	}
}