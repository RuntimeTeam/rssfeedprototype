package com.proto.rss.view
{
	import com.proto.rss.model.FeedVO;
	
	import feathers.controls.Button;
	
	public class RssFeedButton extends Button
	{
		private var _feed:FeedVO;
		public function RssFeedButton(feed:FeedVO)
		{
			super();
			
			_feed = feed;
			this.label = feed.name;
		}

		public function get feed():FeedVO
		{
			return _feed;
		}

	}
}