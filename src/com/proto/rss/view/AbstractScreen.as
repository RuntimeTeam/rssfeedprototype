package com.proto.rss.view
{
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Sprite;
	
	public class AbstractScreen extends Sprite
	{
		public function AbstractScreen()
		{
			super();
			
			this.x = AppConfig.StageWidth;
		}
		
		// **************************************
		//	PUBLIC METHODS
		// **************************************
		
		public function open():void
		{
			Starling.juggler.tween(this, 0.6, {x:0, transition:Transitions.EASE_OUT, onComplete:start, delay:0.5});
		}
		
		public function close():void
		{
			Starling.juggler.tween(this, 0.6, {x:-AppConfig.StageWidth, transition:Transitions.EASE_IN, onComplete:remove});
		}
		
		public function start():void
		{
			
		}
		
		public function init(params:Object):void
		{
			
		}
		
		public function remove():void
		{
			this.removeFromParent(true);
		}
	}
}