package com.proto.rss.view
{
	import feathers.controls.ScrollContainer;
	import feathers.layout.TiledColumnsLayout;
	import feathers.layout.TiledRowsLayout;
	
	public class HScrollContainer extends ScrollContainer
	{
		public function HScrollContainer()
		{
			super();
			
			var layout:TiledRowsLayout = new TiledRowsLayout();
			layout.paging = TiledColumnsLayout.PAGING_NONE;
			layout.horizontalGap = 11;
			layout.verticalGap = 5;
			layout.paddingTop = 0;
			layout.paddingRight = 5;
			layout.paddingBottom = 0;
			layout.paddingLeft = 5;
			layout.horizontalAlign = TiledRowsLayout.HORIZONTAL_ALIGN_LEFT;
			layout.verticalAlign = TiledRowsLayout.VERTICAL_ALIGN_TOP;
			layout.tileHorizontalAlign = TiledRowsLayout.TILE_HORIZONTAL_ALIGN_LEFT;
			layout.tileVerticalAlign = TiledRowsLayout.TILE_VERTICAL_ALIGN_TOP;
			layout.useSquareTiles = false;
			
			this.layout = layout;
			
			this.snapToPages = true;
		}
	}
}