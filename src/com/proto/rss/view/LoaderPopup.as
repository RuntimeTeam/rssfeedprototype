package com.proto.rss.view
{
	import feathers.controls.Label;
	
	import starling.animation.Transitions;
	import starling.core.Starling;
	import starling.display.Quad;
	
	public class LoaderPopup extends AbstractScreen
	{
		private var _q:Quad;
		private var _label:Label;
		public function LoaderPopup()
		{
			super();
			
			this.addChild(_q = new Quad(AppConfig.StageWidth, AppConfig.StageHeight, 0xFFFFFF));
			_q.alpha = 0.8;
			
			this.addChild(_label = new Label());
			_label.text = "Loading feed...";
			
			_label.validate();
			
			_label.x = (AppConfig.StageWidth >> 1) - (_label.width >> 1);
			_label.y = (AppConfig.StageHeight >> 1) - (_label.height >> 1);
			
			this.x = 0;
			this.alpha = 0;
		}
		
		// **************************************
		//	PUBLIC METHODS
		// **************************************
		
		override public function open():void
		{
			Starling.juggler.tween(this, 0.6, {alpha:1, transition:Transitions.EASE_OUT});
		}
		
		override public function close():void
		{
			Starling.juggler.tween(this, 0.6, {alpha:0, transition:Transitions.EASE_OUT});
		}
		
	}
}