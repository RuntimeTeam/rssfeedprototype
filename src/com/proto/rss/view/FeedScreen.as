package com.proto.rss.view
{
	import com.proto.rss.model.FeedItemVO;
	import com.proto.rss.model.FeedVO;
	import com.proto.rss.model.XML2JSON;
	
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import feathers.controls.Button;
	import feathers.controls.ImageLoader;
	
	import starling.events.Event;

	public class FeedScreen extends AbstractScreen
	{
		private var _feed:FeedVO;
		private var _container:HScrollContainer;
		private var _backBtn:Button;
		
		public function FeedScreen()
		{
			super();
			
			this.addChild(_backBtn = new Button());
			_backBtn.label = "back to feeds";
			_backBtn.width = 150;
			_backBtn.height = 70;
			_backBtn.y = 10;
			_backBtn.x = (AppConfig.StageWidth >> 1) - (_backBtn.width >> 1);
			
			_backBtn.addEventListener(Event.TRIGGERED, onTrigger);
			
			this.addChild(_container = new HScrollContainer());
			_container.width = AppConfig.StageWidth;
			_container.y = 100;
			_container.height = AppConfig.StageHeight - _container.y;
			
		}
		
		// **************************************
		//	PUBLIC METHODS
		// **************************************
		
		override public function init(params:Object):void
		{
			_feed = params as FeedVO;
		}
		
		override public function start():void
		{
			if(!_feed) return;
			
			AppRoot.instance.toggleLoading(true);
			
			var ldr:URLLoader = new URLLoader();
			ldr.addEventListener(flash.events.Event.COMPLETE, onComplete);
			ldr.addEventListener(IOErrorEvent.IO_ERROR, onIoError);
			
			var req:URLRequest = new URLRequest(_feed.feedUrl);
			
			ldr.load(req);
		}
		
		// **************************************
		//	PROTECTED METHODS
		// **************************************
		
		protected function onComplete(event:flash.events.Event):void
		{
			AppRoot.instance.toggleLoading(false);
			
			var ldr:URLLoader = event.target as URLLoader;
			var data:Object = XML2JSON.parse(new XML(ldr.data));
			
			var feedItemVO:FeedItemVO;
			for each (var item:Object in data.channel.item) 
			{
				feedItemVO = new FeedItemVO().parse(item);
				_container.addChild(new FeedItem(feedItemVO));
			}
			
			// Load Image
			var imageLdr:ImageLoader = new ImageLoader();
			imageLdr.source = data.channel.image.url || "";
			this.addChild(imageLdr);
			
			
		}
		
		protected function onIoError(event:IOErrorEvent):void
		{
			AppRoot.instance.toggleLoading(false);
		}
		
		// **************************************
		//	PRIVATE METHODS
		// **************************************
		
		private function onTrigger(e:Event):void
		{
			AppRoot.instance.setState(AppRoot.STATE_WELCOME);
		}
	}
}