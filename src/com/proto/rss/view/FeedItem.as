package com.proto.rss.view
{
	import com.proto.rss.model.FeedItemVO;
	
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import feathers.controls.Button;
	import feathers.controls.Label;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class FeedItem extends Sprite
	{
		public static const ITEM_WIDTH:Number = 375;
		public static const ITEM_HEIGHT:Number = 200;
		private var _title:Label;
		private var _feedItem:FeedItemVO;
		private var _desc:Label;
		private var _readBtn:Button;
		
		public function FeedItem(feedItemVO:FeedItemVO)
		{
			super();

			_feedItem = feedItemVO;
			
			_title = new Label();
			_title.nameList.add("feedHeader");
			_title.wordWrap = true;
			_title.width = ITEM_WIDTH;
			this.addChild(_title);
			
			_title.text = _feedItem.title;
			_title.validate();
			
			_desc = new Label();
			_desc.wordWrap = true;
			this.addChild(_desc);
			_desc.width = ITEM_WIDTH;
			_desc.text = _feedItem.description;
			_desc.y = _title.height + 2;
			
			_desc.validate();
			
			this.addChild(_readBtn = new Button());
			_readBtn.label = "view article";
			_readBtn.validate();
			
			_readBtn.x = (ITEM_WIDTH) - (_readBtn.width);
			_readBtn.y = _desc.y + _desc.height + 3;
			
			_readBtn.addEventListener(Event.TRIGGERED, onTrigger);
			
			
		}
		
		// **************************************
		//	PRIVATE METHODS
		// **************************************
		
		private function onTrigger(e:Event):void
		{
			navigateToURL(new URLRequest(_feedItem.link));
			
		}
	}
}