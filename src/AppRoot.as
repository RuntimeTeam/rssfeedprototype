package
{
	import com.proto.rss.view.AbstractScreen;
	import com.proto.rss.view.FeedScreen;
	import com.proto.rss.view.LoaderPopup;
	import com.proto.rss.view.WelcomeScreen;
	
	import starling.display.Sprite;
	import starling.utils.AssetManager;
	
	public class AppRoot extends Sprite
	{
		public static const STATE_WELCOME:String = "welcome";
		public static const STATE_FEEDS:String = "feeds";
		
		public static var assets:AssetManager;
		
		private var _currentScreen:AbstractScreen;
		private var _currentState:String;
		private var _loading:LoaderPopup;
		
		private static var _instance:AppRoot;
		
		public function AppRoot()
		{
			super();
			
			_instance = this;
		}
		
		// **************************************
		//	PUBLIC METHODS
		// **************************************
		
		public static function get instance():AppRoot
		{
			return _instance;
		}

		public function getInstance():AppRoot
		{
			return _instance;
		}
		
		public function start(assets:AssetManager):void
		{
			AppRoot.assets = assets;
			buildUi();
			setState(STATE_WELCOME);
		}
		
		public function setState(state:String, params:Object=null):void
		{
			if(_currentState != state)
			{
				if(_currentScreen) 
				{
					_currentScreen.close();
					_currentScreen = null;
				}
			}
			
			switch(state)
			{
				case STATE_WELCOME:
					_currentScreen = new WelcomeScreen();
					break;
				
				case STATE_FEEDS:
					_currentScreen = new FeedScreen();
					break;
				
				default:
					break;
			}
			
			if(_currentScreen)
			{
				_currentState = state;
				this.addChild(_currentScreen);
				_currentScreen.init(params);
				_currentScreen.open();
			}
		}
		
		public function toggleLoading(toggle:Boolean):void
		{
			if(toggle)
			{
				_loading.open();
			} else
			{
				_loading.close();
			}
		}
		
		// **************************************
		//	PRIVATE METHODS
		// **************************************
		private function buildUi():void
		{
			this.addChild(_loading = new LoaderPopup());
		}
		
	}
}