package
{
	public class AppConfig
	{
		public static const StageWidth:int  = 800;
		public static const StageHeight:int = 600;
		
		public static var FEED_LIST:Array = [{name:"News",feedUrl:"http://feeds.bbci.co.uk/news/rss.xml"},
			{name:"World News",feedUrl:"http://feeds.bbci.co.uk/news/world/rss.xml"},
				{name:"Uk News",feedUrl:"http://feeds.bbci.co.uk/news/uk/rss.xml"},
					{name:"Politics News",feedUrl:"http://feeds.bbci.co.uk/news/politics/rss.xml"},
						{name:"Entertainment and Arts",feedUrl:"http://feeds.bbci.co.uk/news/entertainment_and_arts/rss.xml"},
						{name:"Health",feedUrl:"http://feeds.bbci.co.uk/news/health/rss.xml"},
							{name:"Science and Environment",feedUrl:"http://feeds.bbci.co.uk/news/science_and_environment/rss.xml"}
							]
		
		public function AppConfig()
		{
			
		}
	}
}